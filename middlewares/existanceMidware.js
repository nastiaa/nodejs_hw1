const { isFileExists } = require('../services/file-service');

const existGetFileMidware = (request, response, next) => {
    if(!isFileExists(request.params.filename)) {
       return response.status(400).json({message: `No file with '${request.params.filename}' filename found`});
    }

    next();
}

const existCrFileMidware = (request, response, next) => {
    if(isFileExists(request.body.filename)) {
       return response.status(400).json({message: `File with '${request.body.filename}' filename already exists`});
    }

    next();
}

module.exports = { existGetFileMidware, existCrFileMidware }