const { readDir, readFile, getDate, writeFile, deleteFile, getFileExt } = require('../services/file-service');
const { isValidExtention } = require('../middlewares/validExtMidware');

const getAllFiles = async (request, response) => {
    try {
      const files = await readDir();
      response.status(200).json({ message: 'Success', files});
    } catch (err) {
        response.status(500).json({ message: 'Server error' });
    }
  }

const getFileByName = async (request, response) => {
    try {
        const file = await readFile(request.params.filename);
        const filedate = (await getDate(request.params.filename)).birthtime;
        
        response.status(200).json({
            message: 'Success',
            filename: request.params.filename,
            content: file,
            extension: getFileExt(request.params.filename).replace('.',''),
            uploadedDate: filedate
        });
      } catch (err) {
          response.status(500).json({ message: 'Server error' });
      }
}

const createFile = async (request, response) => {
    try {
       await writeFile(request.body.filename, request.body.content);
        response.status(200).json({message: 'File created successfully'});
      } catch (err) {
          response.status(500).json({ message: 'Server error' });
      }
}

const deleteFileByName = async (request, response) => {
try {
    await deleteFile(request.params.filename);
    response.status(200).json({message: 'File deleted successfully'});
  } catch (err) {
      response.status(500).json({ message: 'Server error' });
      console.log(err)
  }
}


module.exports = { getAllFiles, getFileByName, createFile, deleteFileByName };