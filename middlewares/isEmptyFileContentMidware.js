const isEmptyFileNameMidware = (request, response, next) => {
    if(!request.body.filename) {
       return response.status(400).json({message: 'Please specify \'filename\' parameter'});
    }

    next();
}
 
 const isEmptyFileContentMidware = (request, response, next) => {
    if(!request.body.content) {
       return response.status(400).json({message: 'Please specify \'content\' parameter'});
    }

    next();
}

module.exports = { isEmptyFileNameMidware, isEmptyFileContentMidware }