const { getFileExt } = require('../services/file-service')

const validExtentions = ['.log', '.txt', '.json', '.yaml', '.js', '.xml']

const isValidExtention = filename => validExtentions.includes(getFileExt(filename));

const validExtMidware = (request, response, next) => {
    if(!isValidExtention(request.body.filename)) {
       return response.status(400).json({message: 'Wrong file extention'});
    }

    next();
}

module.exports = { validExtMidware }