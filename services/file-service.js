const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');
const directoryPath = path.join(__dirname, '../files');

const readDir = () => fsPromises.readdir(directoryPath);
const readFile = (filename) => fsPromises.readFile(path.join(directoryPath, filename), 'utf-8');
const getDate = (filename) => fsPromises.stat(path.join(directoryPath, filename), 'utf-8');
const writeFile = (filename, content) => fsPromises.writeFile(path.join(directoryPath, filename), content, 'utf-8');
const isFileExists = (filename) => fs.existsSync(path.join(directoryPath, filename));
const getFileExt = (filename) => path.extname(path.join(directoryPath, filename));
const deleteFile = (filename) => fsPromises.rm(path.join(directoryPath, filename));

module.exports = { readDir, readFile, isFileExists, getDate, getFileExt, writeFile, deleteFile };