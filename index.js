const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const path = require('fs');
const  { isFileExists } = require('./services/file-service');
const fileRouter = require('./routers/file-router');
const PORT = 8080;
const app = express();

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', fileRouter);

app.listen(PORT, () => {
    console.log('Server has been started...');
    if(!isFileExists('')) {
      fs.mkdirSync(`${__dirname}/files`);
    }
});