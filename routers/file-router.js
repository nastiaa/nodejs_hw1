const express = require('express');
const router = express.Router();
const { getAllFiles, getFileByName, createFile, deleteFileByName } = require('../middlewares/fileOperMidware');
const { existGetFileMidware, existCrFileMidware } = require('../middlewares/existanceMidware');
const { validExtMidware } = require('../middlewares/validExtMidware');
const {isEmptyFileNameMidware, isEmptyFileContentMidware } = require('../middlewares/isEmptyFileContentMidware');

router.get('/', getAllFiles);
router.get(`/:filename`, validExtMidware, existGetFileMidware, getFileByName);
router.post(`/`, validExtMidware, existCrFileMidware, isEmptyFileNameMidware, isEmptyFileContentMidware, createFile);
router.delete(`/:filename`, validExtMidware, existGetFileMidware, deleteFileByName);

module.exports = router;
